#!/bin/bash

cd $XDG_DOWNLOAD_DIR
wget "https://www2.ati.com/drivers/linux/amd-catalyst-15.9-linux-installer-15.201.1151-x86.x86_64.zip"
unzip -o amd-catalyst-15.9-linux-installer-15.201.1151-x86.x86_64.zip
chmod +x AMD-Catalyst-15.9-Linux-installer-15.201.1151-x86.x86_64.run

./AMD-Catalyst-15.9-Linux-installer-15.201.1151-x86.x86_64.run
